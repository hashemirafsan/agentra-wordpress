<?php

get_header();

if(have_posts())
	while (have_posts()) : the_post(); ?>
	
	<article class="post">
		<?php bloginfo('name'); ?>
		<?php the_content(); ?>
	</article>

<?php endwhile;

else {
	echo '<p>No content Found</p>';
}


get_footer();
?>