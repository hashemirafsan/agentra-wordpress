<?php 
function Agentra_css()
{
	wp_enqueue_style('style',get_stylesheet_uri());
}
add_action('wp_enqueue_scripts','Agentra_css');

//navigation menu

register_nav_menus( array(


	'primary' => __('Primary Menu'),
	'footer' => __('Footer Menu'),

));

	add_theme_support( 'custom-background', apply_filters( 'Agentra', array(
		'default-color'      => $default_color,
		'default-attachment' => 'fixed',
	) ) );
	
	register_sidebar( array(
		'name'          => __( 'Widget Area', 'twentyfifteen' ),
		'id'            => 'sidebar-1',
		'description'   => __( 'Add widgets here to appear in your sidebar.', 'twentyfifteen' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );