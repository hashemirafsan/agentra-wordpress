<?php

get_header();

if(have_posts())
	while (have_posts()) : the_post(); ?>
	
	<article class="post">
		<h2><a href="<?php the_permalink(); ?>"><?php the_title();?></a></h2>
		<?php the_content(); ?>
		<?php the_content(); ?>
 	</article>

 	<div class="col-md-6 pull-right">
 		<?php get_sidebar(); ?>
 	</div>
<?php endwhile;

else {
	echo '<p>No content Found</p>';
}

	
get_footer();
?>